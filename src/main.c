#include "mem.h"
#include "mem_internals.h"
#include <inttypes.h>
#define heap_size 2048
#define min_block_size 401

bool test1(){
    heap_init(heap_size);
    debug_heap(stdout, HEAP_START);
    void* block = _malloc(min_block_size);
    debug_heap(stdout, HEAP_START);
    if(!block){
        printf("Тест №1 НЕ пройден!!\n");
        return false;
    }
    _free(block);
    debug_heap(stdout, HEAP_START);
    printf("Тест №1 пройден!\n");
    return true;
    }

    bool test2(){
    heap_init(heap_size);
    debug_heap(stdout, HEAP_START);
    void* block1 = _malloc(201);
    void* block2 = _malloc(202);
    if(!block1 || !block2){
        printf("Тест №2 НЕ пройден!!\n");
        return false;
    }
    debug_heap(stdout, HEAP_START);
    _free(block1);
    debug_heap(stdout, HEAP_START);
    if(!block1 || !block2){
        printf("Тест №2 НЕ пройден!\n");
        return false;
    }
    printf("Тест №2 пройден!\n");
    return true;
    }

    bool test3(){
    heap_init(heap_size);
    debug_heap(stdout, HEAP_START);
    void* block1 = _malloc(301);
    void* block2 = _malloc(302);
    void* block3 = _malloc(303);
    if(!block1 || !block2 || !block3){
        printf("Тест №2 НЕ пройден!\n");
        return false;
    }
    debug_heap(stdout, HEAP_START);
    _free(block1);
    debug_heap(stdout, HEAP_START);
    _free(block3);
    debug_heap(stdout, HEAP_START);
    if(!block1 || !block2|| !block3){
        printf("Тест №3 НЕ пройден!\n");
        return false;
    }
    printf("Тест №3 пройден!\n");
    return true;
    }

    bool test4(){
    heap_init(1);
    debug_heap(stdout, HEAP_START);
    void* block1 = _malloc(10288);
    void* block2 = _malloc(5288);
    if(!block1 || !block2){
        printf("Тест №4 НЕ пройден!\n");
        return false;
    }
    _free(block1);
    debug_heap(stdout, HEAP_START);
    printf("Тест №4 пройден!\n");
    return true;
    }

    bool test5(){
    heap_init(1);
    void* block1 = _malloc(10288);
    debug_heap(stdout, HEAP_START);
    if(!block1){
        printf("Тест №5 НЕ пройден!\n");
        return false;
    }
    printf("Тест №5 пройден!\n");
    return true;
    }

int main() {


    if(test1()&&test2()&&test3()&&test4()&&test5()){
        printf("Тесты пройдены\n");
    }else{
        printf("Ошибка тестов\n");
    }

    return 0;
}
